public class Application{
	public static void main(String[] args){
		
		Student ugo = new Student();
		ugo.school = "mariano";
		ugo.rScore = 28.0;
		ugo.age = 18;
		
		Student victor = new Student();
		victor.school = "brebeuf";
		victor.rScore = 12.0;
		victor.age = 32;
		
		Student[] section3 = new Student[3];
		section3[0] = ugo;
		section3[1] = victor;
		section3[2] = new Student();
		
		section3[2].school = "dawson";
		section3[2].rScore = 25.0;
		section3[2].age = 22;
		
		System.out.println(section3[2].school);
		System.out.println(section3[2].age);
		System.out.println(section3[2].rScore);
		
		
	}
	
	
	
}